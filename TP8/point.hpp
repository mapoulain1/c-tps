#ifndef POINT_HPP
#define POINT_HPP

#include <string>

using std::string;

class Point {
  private:
	int x;
	int y;

  public:
	Point(int x, int y);
	Point();
	int getX(void) const;
	int getY(void) const;
	void setX(int _x);
	void setY(int _y);
	string toString() const;
	static const Point ORIGINE;
};

#endif