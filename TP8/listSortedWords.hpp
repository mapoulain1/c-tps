#ifndef LIST_SORTED_WORDS_HPP
#define LIST_SORTED_WORDS_HPP

#include <string>
#include <list>
#include <iterator>
#include <vector>

using std::string;
using std::list;



class ListSortedWords {
private:
    list<string> words;
public:
    ListSortedWords();
    void addWord(const string &word);
    void removeWord(const string &word);
    void insert(const std::list<string>::const_iterator &start, const std::list<string>::const_iterator &end);
    int getNumberOfWords(void) const;
    std::list<string>::const_iterator begin(void) const;
    std::list<string>::const_iterator end(void) const;
    using const_iterator = std::list<string>::const_iterator;
};



#endif