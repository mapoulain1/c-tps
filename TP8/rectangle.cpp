#include "rectangle.hpp"
#include "forme.hpp"
#include <sstream>

using std::string;
using std::stringstream;

Rectangle::Rectangle() : Rectangle(0,0,0,0){
}

Rectangle::Rectangle(int x, int y, int w, int h) : Forme(x, y, w, h) {
}

Rectangle::~Rectangle(){
}

string Rectangle::toString() {
	stringstream ss;
	ss << "RECTANGLE " << getPosition().toString() << " " << getW() << " " << getH();
	return ss.str();
}

Rectangle* Rectangle::clone(void){
	return new Rectangle(getPosition().getX(), getPosition().getY(),getW(), getH());
}