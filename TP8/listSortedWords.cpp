#include "listSortedWords.hpp"
#include <algorithm>

#include <iostream>

ListSortedWords::ListSortedWords() : words() {
}

void ListSortedWords::addWord(const string &word) {
    auto position = std::find_if(words.begin(), words.end(),
                                 [word](string &w) {
                                     return w > word;
                                 });
    words.insert(position, word);
}

void ListSortedWords::removeWord(const string &word) {
    words.remove_if(
        [word](string w) {
            return word == w;
        });
}

void ListSortedWords::insert(const std::list<string>::const_iterator &start, const std::list<string>::const_iterator &end) {
    auto v = *start;
    auto position = std::find_if(words.begin(), words.end(),
                              [&v](string &w) {
                                  return w < v;
                              });
    words.insert(position, start, end);
}

int ListSortedWords::getNumberOfWords(void) const {
    return words.size();
}

std::list<string>::const_iterator ListSortedWords::begin(void) const {
    return words.begin();
}

std::list<string>::const_iterator ListSortedWords::end(void) const {
    return words.end();
}
