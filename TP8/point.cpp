#include "point.hpp"

#include <sstream>

using std::stringstream;

Point::Point(int x, int y) : x(x), y(y) {
}
Point::Point() : Point(0, 0) {
}
int Point::getX(void) const {
	return x;
}
int Point::getY(void) const {
	return y;
}
void Point::setX(int _x) {
	x = _x;
}
void Point::setY(int _y) {
	y = _y;
}

string Point::toString() const {
	stringstream ss;
	ss << "{" << x << "," << y << "}";
	return ss.str();
}

const Point Point::ORIGINE = Point{};
