#ifndef DICTONARY_HPP
#define DICTONARY_HPP

#include "listSortedWords.hpp"
#include <map>
#include <string>

using std::map;
using std::string;


class Dictionary{
private:
    map<char,ListSortedWords> map;
public:
    Dictionary();
    void addWord(const string &word);
    void removeWord(const string &word);
    int getNumberOfWords(void) const;
    ListSortedWords search(const string & word);
};


#endif