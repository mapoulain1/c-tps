#include "groupe.hpp"
#include <sstream>
#include <string>

using std::abs;
using std::max;
using std::min;

using std::endl;
using std::string;
using std::stringstream;

Groupe::Groupe() : Forme(0, 0, 0, 0), formes() {}

Groupe::~Groupe() {
  for (auto forme : formes)
    delete forme;
}

void Groupe::add(Forme *forme) {
  setW(max(getW(), abs(forme->getW() + forme->getPosition().getX())));
  setH(max(getH(), abs(forme->getH() + forme->getPosition().getY())));
  getPosition().setX(min(getPosition().getX(), forme->getPosition().getX()));
  getPosition().setY(min(getPosition().getY(), forme->getPosition().getY()));
  formes.push_back(forme);
}

string Groupe::toString(void) {
  stringstream ss;
  ss << "GROUPE " << getPosition().toString() << " " << getW() << " " << getH()
     << endl;
  for (Forme *forme : formes) {
    ss << "\t" << forme->toString() << endl;
  }
  return ss.str();
}

Forme *Groupe::operator[](std::size_t i) { return formes[i]; }

const Forme *Groupe::operator[](std::size_t i) const { return formes[i]; }

std::size_t Groupe::size(void) const { return formes.size(); }

Groupe *Groupe::clone(void) {
  Groupe *groupe = new Groupe();
  for (auto forme : formes)
    groupe->add(forme->clone());
  return groupe;
}