#include <iostream>
#include <string>

#include "catch.hpp"
#include "cercle.hpp"
#include "groupe.hpp"
#include "listSortedWords.hpp"

using std::cout;
using std::endl;
using std::string;

TEST_CASE("Test clone") {
    Groupe groupe{};
    groupe.add(new Cercle(0, 0, 10));
    groupe.add(new Cercle(-5, 2, 5));
    groupe.add(new Cercle(1, 3, 3));
    cout << groupe.toString() << endl;

    Groupe *cloned = groupe.clone();
    cout << cloned->toString() << endl;

    delete cloned;
}

TEST_CASE("Test lambda") {
    string word = "c";
    vector<string> v;
    v.push_back("a");
    v.push_back("b");

    v.push_back("d");
    v.push_back("e");

    auto lambda = [word](string &w) {
        cout << "comparing '" << w << "' and '" << word << "'" << endl;
        return w > word;
    };

    auto res = find_if(v.begin(), v.end(), lambda);
    if (res != v.end()) {
        cout << "insert at : " << *res << endl;
    }
}

TEST_CASE("ListeMT1") {
    ListSortedWords liste;

    REQUIRE(0 == liste.getNumberOfWords());
    liste.addWord("inserer");
    REQUIRE(1 == liste.getNumberOfWords());
    liste.addWord("effacer");
    REQUIRE(2 == liste.getNumberOfWords());
    liste.addWord("ajout");
    REQUIRE(3 == liste.getNumberOfWords());

    ListSortedWords::const_iterator it = liste.begin();

    REQUIRE(*it == "ajout");
    ++it;
    REQUIRE(*it == "effacer");
    ++it;
    REQUIRE(*it == "inserer");
    ++it;
    REQUIRE(it == liste.end());

    liste.removeWord("effacer");
    REQUIRE(2 == liste.getNumberOfWords());
    liste.removeWord("inserer");
    REQUIRE(1 == liste.getNumberOfWords());
    liste.removeWord("ajout");
    REQUIRE(0 == liste.getNumberOfWords());

    REQUIRE(liste.begin() == liste.end());
}

TEST_CASE("ListeMT2") {
    ListSortedWords liste1, liste2;

    liste1.addWord("essai 1 a");
    liste1.addWord("essai 1 b");
    liste2.insert(liste1.begin(), liste1.end());

    ListSortedWords liste3;
    liste3.addWord("essai 1 b");
    liste3.addWord("essai 2 a");
    liste3.addWord("essai 2 c");
    liste2.insert(liste3.begin(), liste3.end());
}