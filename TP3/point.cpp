#include "point.hpp"

Point::Point(int x, int y) : x(x), y(y) {
}
Point::Point() : Point(0, 0) {
}
int Point::getX(void) {
	return x;
}
int Point::getY(void) {
	return y;
}
void Point::setX(int _x) {
	x = _x;
}
void Point::setY(int _y) {
	y = _y;
}
