#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <string>

using std::string;

class Rectangle {
  private:
	int x;
	int y;
	int w;
	int h;
	int index;

  public:
	Rectangle(int x, int y, int w, int h);
	string toString();
	int getIndex(void);
	void setIndex(int index);

};

#endif