#include "classA.hpp"

ClassA::ClassA(int value) : i(value) {
}

void ClassA::exec(int toAdd) {
	i += toAdd;
}

void ClassA::send(ClassB *b) {
	b->exec(10);
}

int ClassA::getValue(void){
    return i;
}