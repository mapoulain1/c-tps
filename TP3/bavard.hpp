#ifndef BAVARD_HPP
#define BAVARD_HPP

class Bavarde {
  private:
	int value;

  public:
	Bavarde(int value = 0);
	~Bavarde();
	int get(void);
	void affiche(void);
};

void fonction(Bavarde b);

#endif