#include <fstream>
#include <iostream>

#include "catch.hpp"
#include "forme.hpp"
#include "liste.hpp"
#include "producteur.hpp"
#include "producteur_entiers.hpp"
#include "statisticien.hpp"

TEST_CASE("Producteur_Initialisation") {
	Producteur *p = new ProducteurEntier();
	REQUIRE(p->getTravail() == 0);
	delete p;
}

TEST_CASE("Producteur_travail2") {
	Producteur *p = new ProducteurEntier();
	p->produire(10, "test01.txt");
	p->produire(10, "test01.txt");
	p->produire(10, "test01.txt");
	REQUIRE(p->getTravail() == 3);
	delete p;
}

TEST_CASE("Producteur_Travail3") {
	const int DEMANDE = 10;
	const std::string NOM_FICHIER("test01.txt");
	int lecture, i;
	Producteur *p = new ProducteurEntier();
	p->produire(DEMANDE, NOM_FICHIER.c_str());
	std::ifstream fichier(NOM_FICHIER.c_str());
	REQUIRE(fichier.is_open());
	if (!fichier.eof()) {
		fichier >> lecture;
		REQUIRE(DEMANDE == lecture);
		for (i = 0; i < DEMANDE; ++i) {
			fichier >> lecture;
			REQUIRE(lecture == (i + 1));
		}
		REQUIRE(i == DEMANDE);
		fichier.close();
		REQUIRE(p->getTravail() == 1);
	}
	delete p;
}

TEST_CASE("Statisticien_Initialisation") {
	const std::string NOM_FICHIER("test01.txt");
	Statisticien p;
	p.acquerir(NOM_FICHIER);
	CHECK(p.getCalcul());
	if (p.getCalcul()) {
		REQUIRE(p.getSum() == 55);
		REQUIRE(p.getAverage() == 5.5);
	}
}
TEST_CASE("Fils_Rouge") {
	Liste liste;
	Cercle c1{0, 0, 1};
	Cercle c2{0, 0, 2};
	Cercle c3{0, 0, 3};
	Rectangle r1{0, 0, 1, 1};
	Rectangle r2{0, 0, 2, 2};
	Rectangle r3{0, 0, 3, 3};
	Rectangle r4{0, 0, 4, 4};
	Rectangle r5{0, 0, 5, 5};
	liste.add(c2);
	liste.add(r1);
	liste.add(c3);
	liste.add(r5);
	liste.add(r3);
	liste.add(c1);
	liste.add(r2);
	liste.add(r4);

	REQUIRE(c2.getIndex() == 1);
	REQUIRE(r1.getIndex() == 2);
	REQUIRE(c3.getIndex() == 3);
	REQUIRE(r5.getIndex() == 4);
	REQUIRE(r3.getIndex() == 5);
	REQUIRE(c1.getIndex() == 6);
	REQUIRE(r2.getIndex() == 7);
	REQUIRE(r4.getIndex() == 8);
	//liste.afficher();
}

TEST_CASE("Fils_Rouge_Formes") {
	REQUIRE(Forme::getNbFormes() == 0);

	Forme f1{0, 1, 2, 3};
	Forme f2{0, 1, Point{0, 0}};
	Forme f3;

	REQUIRE(Forme::getNbFormes() == 3);
}
