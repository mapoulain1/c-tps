#include <iostream>

#include "bavard.hpp"
using std::cout;
using std::endl;

Bavarde::Bavarde(int value) : value(value) {
	cout << "Construction de " << value << endl;
}

Bavarde::~Bavarde() {
	cout << "Destruction de " << value << endl;
}

int Bavarde::get(void) {
	return value;
}

void Bavarde::affiche(void) {
	cout << "Affiche " << value << endl;
}

void fonction(Bavarde b) {
	cout << "code de la fonction " << b.get() << endl;
}

//Bavarde globale(39);