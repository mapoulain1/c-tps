#ifndef CLASSA_HPP
#define CLASSA_HPP

#include "classB.hpp"

class ClassA {
  private:
	int i;

  public:
	ClassA(int value);
	void send(ClassB *b);
	void exec(int toAdd);
    int getValue(void);
};

#endif