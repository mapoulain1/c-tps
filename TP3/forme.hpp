#ifndef FORME_HPP
#define FORME_HPP

#include "point.hpp"

class Forme {
  private:
	static int nbFormes;
	int w;
	int h;
	Point position;

  public:
	Forme();
	Forme(int w, int h, int x, int y);
	Forme(int w, int h, Point position);
    static int getNbFormes(void);
};

#endif