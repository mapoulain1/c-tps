#ifndef PRODUCTEUR_ENTIERS_HPP
#define PRODUCTEUR_ENTIERS_HPP

#include "producteur.hpp"

class ProducteurEntier : public Producteur {
  public:
	virtual bool produire(int quantite, std::string nom);
};

#endif