#include "liste.hpp"

#include <iostream>

using std::cout;
using std::endl;

Liste::Liste() : rectangles(), cercles(), lastIndex(0), lastIndexRectangles(0), lastIndexCercles(0) {
}

Liste::~Liste() {
}

void Liste::add(Cercle &cercle) {
	lastIndex++;
	lastIndexCercles++;
	cercle.setIndex(lastIndex);
	cercles.push_back(cercle);
}

void Liste::add(Rectangle &rectangle) {
	lastIndex++;
	lastIndexRectangles++;
	rectangle.setIndex(lastIndex);
	rectangles.push_back(rectangle);
}

void Liste::afficher(void) {
	int currentCercle = 0;
	int currentRectangle = 0;

	while (currentCercle < lastIndexCercles && currentRectangle < lastIndexRectangles) {
		if (cercles[currentCercle].getIndex() < rectangles[currentRectangle].getIndex()) {
			cout << cercles[currentCercle].toString() << endl;
			currentCercle++;
		} else {
			cout << rectangles[currentRectangle].toString() << endl;
			currentRectangle++;
		}
	}
	while (currentCercle < lastIndexCercles) {
		cout << cercles[currentCercle].toString() << endl;
		currentCercle++;
	}
	while (currentRectangle < lastIndexRectangles) {
		cout << rectangles[currentRectangle].toString() << endl;
		currentRectangle++;
	}
}