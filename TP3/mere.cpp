#include "mere.hpp"

#include <iostream>

using std::cout;
using std::endl;

int Mere::value = 0;

Mere::Mere(string name) : name(name) {
    value++;
	cout << "ctor Mere "  << name << endl;
}

Mere::~Mere() {
	cout << "dtor Mere" << endl;
}

int Mere::getConteur(void){
    return value;
}


string Mere::getName(void){
    return name;
}

void Mere::afficher(void){
    cout << "Afficher depuis Mere" << endl;
}