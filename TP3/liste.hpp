#ifndef LISTE_HPP
#define LISTE_HPP

#include "rectangle.hpp"
#include "cercle.hpp"
#include <vector>

using std::vector;

class Liste {
  private:
	vector<Rectangle> rectangles;
	vector<Cercle> cercles;
	int lastIndex;
	int lastIndexRectangles;
	int lastIndexCercles;
	

  public:
	Liste();
	~Liste();
	void add(Rectangle &rectangle);
	void add(Cercle &cercle);
	void afficher(void);

};

#endif