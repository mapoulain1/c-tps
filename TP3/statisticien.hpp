#ifndef __CPP3_STATISTICIEN_HPP__
#define __CPP3_STATISTICIEN_HPP__

#include <iostream>

class Statisticien {
  private:
	bool calcul;
    int sum;
    double average;

  public:
    Statisticien();
	void acquerir(std::string nom);
    bool getCalcul(void);
    int getSum(void);
    double getAverage(void);
    
};

#endif