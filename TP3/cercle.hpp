#ifndef CERCLE_HPP
#define CERCLE_HPP

#include <string>

using std::string;

class Cercle {
  private:
	int x;
	int y;
	int w;
	int h;
	int index;

  public:
	Cercle(int x, int y, int w, int h);
	Cercle(int x, int y, float rayon);
	string toString();
	int getIndex(void);
	void setIndex(int index);
};

#endif