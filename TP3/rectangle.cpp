#include "rectangle.hpp"
#include <sstream>

using std::string;
using std::stringstream;

Rectangle::Rectangle(int x, int y, int w, int h) : x(x), y(y), w(w), h(h), index(0) {
}

string Rectangle::toString() {
	stringstream ss;
	ss << "RECTANGLE " << index << " : " << x << " " << y << " : " << w << " " << h;
	return ss.str();
}

int Rectangle::getIndex(void) {
	return index;
}

void Rectangle::setIndex(int index) {
	this->index = index;
}