#ifndef FILLE_HPP
#define FILLE_HPP

#include "mere.hpp"

class Fille : public Mere {
  public:
	Fille(string name);
	~Fille();
	void afficher(void) override;
};

#endif