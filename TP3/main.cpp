#include <cstdlib>
#include <iostream>

#include "bavard.hpp"
#include "classA.hpp"
#include "classB.hpp"
#include "couple.hpp"
#include "famille.hpp"
#include "fille.hpp"
#include "mere.hpp"



using std::cout;
using std::endl;

void exerciceBavarde(void) {
	Bavarde bizarre(2);
	Bavarde b;
	Bavarde *p = new Bavarde(3);
	delete p;
	fonction(bizarre);
	cout << Bavarde(-1000).get() << endl;
}

void exerciceTableaux(void) {
	const int TAILLE = 20;
	Bavarde tab1[TAILLE];
	Bavarde *tab2 = new Bavarde[TAILLE];

	for (int i = 0; i < TAILLE; ++i) {
		tab1[i].affiche();
		tab2[i].affiche();
	}
	delete[] tab2;
}

void exerciceCouple(void) {
	Couple c{1, 2};
}

void exerciceFamille(void) {
	Famille f{3};
}

void exerciceMalloc(void) {
	Bavarde *b;
	b = (Bavarde *)malloc(sizeof(Bavarde));
	new (b) Bavarde(84);
	b->affiche();
	delete b;
}

void exerciceHeritage(void) {
	cout << Mere::getConteur() << endl;
	Mere m{"Maman"};
	Fille f{"Fille"};
	cout << m.getConteur() << endl;
	cout << f.getConteur() << endl;

	Mere *pm = new Mere("mere_dyn");
	Fille *pf = new Fille("fille_dyn");
	Mere *pp = new Fille("fille vue comme mere");
	pm->afficher(); // affiche Mere
	pf->afficher(); // affiche Fille
	pp->afficher(); // affiche Fille
	delete pm;
	delete pf;
	delete pp;
}

void exerciceAB(void) {
	ClassA a = ClassA(1);
	ClassB b = ClassB(2);
	cout << "a : " << a.getValue() << "\tb : " << b.getValue() << endl;
	a.send(&b);
	b.send(&a);
	cout << "a : " << a.getValue() << "\tb : " << b.getValue() << endl;
}
/*
int main(int, char **) {
	exerciceTableaux();
	exerciceBavarde();
	exerciceCouple();
	exerciceFamille();
	exerciceMalloc();
	exerciceHeritage();
	exerciceAB();
	return 0;
}
*/