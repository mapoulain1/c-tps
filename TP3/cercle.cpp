#include "cercle.hpp"

#include <string>
#include <sstream>

using std::string;
using std::stringstream;

Cercle::Cercle(int x, int y, int w, int h) : x(x), y(y), w(w), h(h), index(0) {
}

Cercle::Cercle(int x, int y, float rayon) : Cercle(x,y,2*rayon,2*rayon) {
}


string Cercle::toString(){
    stringstream ss;
	ss << "CERCLE " << index << " : " << x << " " << y << " : " << w << " " << h;
	return ss.str();
}


int Cercle::getIndex(void){
	return index;
}

void Cercle::setIndex(int index){
	this->index = index;
}