#ifndef POINT_HPP
#define POINT_HPP

class Point {
  private:
	int x;
	int y;

  public:
	Point(int x, int y);
	Point();
	int getX(void);
	int getY(void);
	void setX(int _x);
	void setY(int _y);
};

#endif