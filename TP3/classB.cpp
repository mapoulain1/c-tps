#include "classB.hpp"
#include "classA.hpp"

ClassB::ClassB(int value) : j(value) {
}

void ClassB::exec(int toAdd) {
	j += toAdd;
}

void ClassB::send(ClassA *a) {
	a->exec(12);
}

int ClassB::getValue(void){
    return j;
}