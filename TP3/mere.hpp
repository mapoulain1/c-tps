#ifndef MERE_HPP
#define MERE_HPP

#include <string>

using std::string;

class Mere {
  private:
	static int value;
	string name;

  public:
	Mere(string name);
	virtual ~Mere();
	static int getConteur(void);
	string getName(void);
	virtual void afficher(void);
};

#endif