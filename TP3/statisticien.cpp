#include "statisticien.hpp"
#include <fstream>

Statisticien::Statisticien() : calcul(false), sum(0), average(0) {
}

void Statisticien::acquerir(std::string nom) {
	std::ifstream file;
	int i = 0, max;
	int total = 0;

	file.open(nom.c_str());
	if (!file.fail()) {

		file >> max;

		while (!file.eof() && i < max) {
			double lecture;
			file >> lecture;
			++i;
			total += lecture;
		}
		file.close();
		calcul = true;
		sum = total;
		average = (double)total / max;
	}
}

bool Statisticien::getCalcul(void) {
	return calcul;
}
int Statisticien::getSum(void) {
	return sum;
}
double Statisticien::getAverage(void) {
	return average;
}
