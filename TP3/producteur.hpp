#ifndef PRODUCTEUR_HPP
#define PRODUCTEUR_HPP

#include <iostream>

class Producteur {
  protected:
	unsigned int travail;

  public:
	Producteur();
	virtual ~Producteur();
  int getTravail(void);
	virtual bool produire(int quantite, std::string nom) = 0;

};

#endif
