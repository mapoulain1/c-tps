#ifndef FAMILLE_HPP
#define FAMILLE_HPP

#include "bavard.hpp"

class Famille {
  private:
	Bavarde *array;

  public:
	Famille(unsigned int number);
	~Famille();
	Bavarde &operator[](unsigned int x);
};

#endif