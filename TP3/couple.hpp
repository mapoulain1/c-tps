#ifndef COUPLE_HPP
#define COUPLE_HPP

#include "bavard.hpp"

class Couple {
  private:
	Bavarde bavarde1;
	Bavarde bavarde2;

  public:
	Couple(int value1, int value2);
};

#endif