#ifndef CLASSB_HPP
#define CLASSB_HPP

class ClassA;
class ClassB {
  private:
	int j;

  public:
    ClassB(int value);
    void send(ClassA *a);
    void exec(int toAdd);
    int getValue(void);
};

#endif