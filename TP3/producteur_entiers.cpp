#include "producteur_entiers.hpp"

#include <iostream>
#include <fstream>


bool ProducteurEntier::produire(int quantite, std::string nom) {
	travail++;
	std::ofstream file(nom.c_str());
	if (!file.fail()) {
		file << quantite << std::endl;
		for (int i = 0; i < quantite; ++i)
			file << i + 1 << std::endl;
		file.close();
        return true;
	}
    return false;
}