#include "fille.hpp"

#include <iostream>

using std::cout;
using std::endl;

Fille::Fille(string name) : Mere(name) {
	cout << "ctor Fille " << name << endl;
}

Fille::~Fille() {
	cout << "dtor Fille" << endl;
}



void Fille::afficher(void){
    cout << "Afficher depuis Fille" << endl;
}