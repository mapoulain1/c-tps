#include "forme.hpp"

Forme::Forme() : Forme(0, 0, 0, 0) {
}

Forme::Forme(int w, int h, int x, int y) : Forme(w, h, Point{x, y}) {
}

Forme::Forme(int w, int h, Point position) : w(w), h(h), position(position) {
    nbFormes++;
}

int Forme::nbFormes = 0;

int Forme::getNbFormes(void){
    return nbFormes;
}