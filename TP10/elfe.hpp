#ifndef ELFE_HPP
#define ELFE_HPP

#include "individus.hpp"
#include <string>

using std::string;

class Elfe : public Individus {
  public:
    Elfe(string nom);
    ~Elfe();
};

#endif