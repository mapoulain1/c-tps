#ifndef MACHINE_HPP
#define MACHINE_HPP

#include <string>

using std::string;

class Machine {
  private:
    string type;
    int autonomie;
    int ifixit;

  public:
    Machine(string type, int autonomie, int ifixit);
    ~Machine();
    string getType() const;
    int getAutonomie() const;
    int getIfixit() const;

    
};

#endif