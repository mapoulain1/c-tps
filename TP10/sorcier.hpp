#ifndef SORCIER_HPP
#define SORCIER_HPP

class Sorcier {
  public:
    virtual void ensorceler() const = 0;
};

#endif