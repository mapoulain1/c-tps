#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include "humain.hpp"
#include "humanoide.hpp"
#include "machine.hpp"
#include "sorcier.hpp"
#include "shaman.hpp"
#include "mage.hpp"

using std::cout;
using std::endl;
using std::string;
using std::vector;

TEST_CASE("Humain1") {
    const char *nom = "Alex";
    const Humain alex(nom, 35, genre_t::HOMME);

    CHECK(nom == alex.getNom());
    CHECK(HOMME == alex.getGenre());
    // ou enum class Genre.HOMME
    CHECK(35 == alex.getAge());
}

TEST_CASE("Humain2") {
    Humain thomas("thomas", 26, genre_t::HOMME);

    thomas.setNom("conchita");
    thomas.setAge(27);
    thomas.setGenre(FEMME);

    CHECK("conchita" == thomas.getNom());
    CHECK(FEMME == thomas.getGenre());
    CHECK(27 == thomas.getAge());
}

TEST_CASE("Machine") {
    Machine stylet("stylet Apple", 2 * 24 * 3600, 1);

    CHECK("stylet Apple" == stylet.getType());
    CHECK(2 * 24 * 3600 == stylet.getAutonomie());
    CHECK(1 == stylet.getIfixit());
}

TEST_CASE("Robocop") {
    Humanoide robocop("Murphy", "Robocop 1.0", 35, genre_t::HOMME);

    CHECK("Murphy" == robocop.getNom());
    CHECK("Robocop 1.0" == robocop.getType());
    CHECK(HOMME == robocop.getGenre());
    CHECK(35 == robocop.getAge());
    CHECK(1 == robocop.getIfixit());
}

TEST_CASE("Sorcier") {
    vector<Sorcier*> v;
    v.push_back(new Shaman("Sinaro"));
    v.push_back(new Shaman("Maxime"));
    v.push_back(new Mage("Michel"));

    for(auto sorcier : v){
      sorcier->ensorceler();
    }
}