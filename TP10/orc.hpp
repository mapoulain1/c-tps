#ifndef ORC_HPP
#define ORC_HPP

#include "individus.hpp"
#include <string>

using std::string;

class Orc : public Individus {
  public:
    Orc(string nom);
    ~Orc();
};

#endif