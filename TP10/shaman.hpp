#ifndef SHAMAN_HPP
#define SHAMAN_HPP

#include "elfe.hpp"
#include "sorcier.hpp"
#include <string>

using std::string;


class Shaman : public Elfe, public Sorcier {
  public:
    Shaman(string nom);
    virtual void ensorceler() const override;
};

#endif