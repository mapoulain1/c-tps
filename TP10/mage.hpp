#ifndef MAGE_HPP
#define MAGE_HPP

#include "humain2.hpp"
#include "sorcier.hpp"
#include <string>

using std::string;

class Mage : public Humain2, public Sorcier{
public:
    Mage(string nom);
    ~Mage();

    virtual void ensorceler() const override;
};

#endif