#include "humain.hpp"

Humain::Humain(string nom, int age, genre_t genre) : nom(nom), age(age), genre(genre) {
}

Humain::~Humain() {
}

string Humain::getNom() const {
    return nom;
}

void Humain::setNom(const string &_nom) {
    nom = _nom;
}

int Humain::getAge() const {
    return age;
}

void Humain::setAge(const int _age) {
    age = _age;
}

genre_t Humain::getGenre() const {
    return genre;
}

void Humain::setGenre(const genre_t &_genre) {
    genre = _genre;
}