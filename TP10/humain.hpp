#ifndef HUMAIN_HPP
#define HUMAIN_HPP

#include <string>

using std::string;

enum genre_t {
    HOMME,
    FEMME
};

class Humain {
  private:
    string nom;
    int age;
    genre_t genre;

  public:
    Humain(string nom, int age, genre_t genre);
    ~Humain();
    string getNom() const;
    void setNom(const string &_nom);

    int getAge() const;
    void setAge(const int _age);

    genre_t getGenre() const;
    void setGenre(const genre_t& _genre);
};

#endif