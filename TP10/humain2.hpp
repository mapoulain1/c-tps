#ifndef HUMAIN2_HPP
#define HUMAIN2_HPP

#include "individus.hpp"
#include <string>

using std::string;

class Humain2 : public Individus {
  public:
    Humain2(string nom);
    virtual ~Humain2();
};

#endif