#include "machine.hpp"

Machine::Machine(string type, int autonomie, int ifixit) : type(type), autonomie(autonomie), ifixit(ifixit) {
}

Machine::~Machine() {
}

string Machine::getType() const {
    return type;
}

int Machine::getAutonomie() const {
    return autonomie;
}

int Machine::getIfixit() const {
    return ifixit;
}
