#ifndef HUMANOIDE_HPP
#define HUMANOIDE_HPP

#include <string>

#include "humain.hpp"
#include "machine.hpp"

using std::string;

class Humanoide : public Humain, public Machine {
  public:
    Humanoide(string name, string type, int age, genre_t genre);
    ~Humanoide();
};

#endif