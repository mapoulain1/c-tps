#ifndef INDIVIDUS_HPP
#define INDIVIDUS_HPP

#include <string>

using std::string;

class Individus {
  private:
    string nom;
    int vitesse;
    int force;
    int vitalite;
    int magie;

  protected:
    Individus(string nom, int vitesse, int force, int vitalite, int magie);
  public:
    virtual ~Individus();
};

#endif