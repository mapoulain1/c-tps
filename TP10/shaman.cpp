#include "shaman.hpp"
#include <iostream>

using std::cout;
using std::endl;

Shaman::Shaman(string nom) : Elfe(nom) {
}

void Shaman::ensorceler() const {
    cout << "<Shaman> : Je t'ensorcèle vilain garnement" << endl;
}
