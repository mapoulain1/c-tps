#ifndef SOLDAT_HPP
#define SOLDAT_HPP

class Soldat{
public:
    virtual void combattre() = 0;
};
#endif