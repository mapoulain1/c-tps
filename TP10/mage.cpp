#include "mage.hpp"
#include <iostream>

using std::cout;
using std::endl;

Mage::Mage(string nom) : Humain2(nom) {
}

Mage::~Mage() {
}

void Mage::ensorceler() const {
    cout << "<Mage> : Je t'ensorcèle vilain garnement" << endl;
}
