#ifndef POINT_HPP
#define POINT_HPP

class Point {
  private:
	int x;
	int y;
	static int counter;

  public:
	Point(int x, int y);
	Point();

	int getX(void);
	int getY(void);
	void setX(int x);
	void setY(int y);
	void moveTo(int x, int y);
	void moveBy(int x, int y);
	static int getCounter(void);
};

#endif