#include <iostream>

#include "cercle.hpp"
#include "point.hpp"
#include "rectangle.hpp"

using std::cout;
using std::endl;

void testPoints(void) {
	cout << "Number of points : " << Point::getCounter() << endl;

	Point p{0, 0};
	Point p2;
	Point *p3 = new Point();
	delete p3;

	cout << p.getX() << "\t:\t" << p.getY() << endl;

	p.setX(100);
	p.setY(10);

	cout << p.getX() << "\t:\t" << p.getY() << endl;
	p.moveTo(50, 60);

	cout << p.getX() << "\t:\t" << p.getY() << endl;
	p.moveBy(5, 5);

	cout << p.getX() << "\t:\t" << p.getY() << endl;

	cout << "Number of points (static) : " << Point::getCounter() << endl;
	cout << "Number of points (object) : " << p2.getCounter() << endl;
}

void testCercle(void) {
	Cercle c1{0, 0, 4};
	Cercle c2{0, 0, -1, 1};

	cout << c1.toString() << endl;
	cout << c2.toString() << endl;
}

void testRectangle(void) {
	Rectangle r1{0, 0, 3, 5};
	Rectangle r2{-1, 3, 4, 2};

	cout << r1.toString() << endl;
	cout << r2.toString() << endl;
}

int main(int, char **) {
	//testPoints();
	testCercle();
	testRectangle();
	return 0;
}