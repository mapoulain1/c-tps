#include "point.hpp"
#include <iostream>

using std::cout;
using std::endl;

int Point::counter=0;

Point::Point(int x, int y) : x(x), y(y) {
	cout << "ctor Point(x,y)" << endl;
    counter++;
}

Point::Point() : Point(0, 0) {
    cout << "ctor Point()" << endl;
}

int Point::getX(void) {
	return x;
}

int Point::getY(void) {
	return y;
}

void Point::setX(int x) {
	this->x = x;
}
void Point::setY(int y) {
	this->y = y;
}

void Point::moveTo(int x, int y) {
	setX(x);
	setY(y);
}

void Point::moveBy(int x, int y) {
	setX(x + getX());
	setY(y + getY());
}

int Point::getCounter(void){
    return counter;
}
