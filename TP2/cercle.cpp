#include "cercle.hpp"

#include <string>
#include <sstream>

using std::string;
using std::stringstream;

Cercle::Cercle(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) {
}

Cercle::Cercle(int x, int y, float rayon) : x(x), y(y), w(2 * rayon), h(2 * rayon) {
}


string Cercle::toString(){
    stringstream ss;
	ss << "CERCLE " << x << " " << y << " : " << w << " " << h;
	return ss.str();
}
