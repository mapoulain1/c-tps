#include "rectangle.hpp"
#include <sstream>

using std::string;
using std::stringstream;

Rectangle::Rectangle(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) {
}

string Rectangle::toString() {
	stringstream ss;
	ss << "RECTANGLE " << x << " " << y << " : " << w << " " << h;
	return ss.str();
}
