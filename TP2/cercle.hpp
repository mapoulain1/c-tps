#ifndef CERCLE_HPP
#define CERCLE_HPP

#include <string>

using std::string;

class Cercle {
  private:
	int x;
	int y;
	int w;
	int h;

  public:
	Cercle(int x, int y, int w, int h);
	Cercle(int x, int y, float rayon);
	string toString();
};

#endif