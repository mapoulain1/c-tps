#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <string>

using std::string;

class Rectangle {
  private:
	int x;
	int y;
	int w;
	int h;

  public:
	Rectangle(int x, int y, int w, int h);
	string toString();
};

#endif