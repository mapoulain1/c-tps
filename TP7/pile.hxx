#include "pile.hpp"
#include <stdexcept>

template <class T>
Pile<T>::Pile() : Pile(10) {
}

template <class T>
Pile<T>::Pile(int capacity) : array(nullptr), capacity(capacity), topIndex(0) {
	if (capacity <= 0)
		throw std::invalid_argument("capacity can't be negative or null");
	array = new T[capacity];
}

template <class T>
Pile<T>::~Pile() {
	delete[] array;
}

template <class T>
bool Pile<T>::empty(void) const {
	return topIndex == 0;
}

template <class T>
void Pile<T>::push(const T& value) {
	if (topIndex >= capacity)
		throw std::overflow_error("stack is full");
	array[topIndex] = value;
	topIndex++;
}

template <class T>
T& Pile<T>::pop(void) {
	if (topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	topIndex--;
	return array[topIndex + 1];
}

template <class T>
T& Pile<T>::top(void) const {
	if (topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	return array[topIndex - 1];
}

template <class T>
int Pile<T>::size(void) const {
	return topIndex;
}
