#ifndef FORME_HPP
#define FORME_HPP

#include "point.hpp"

enum class Couleur {
	NOIR,
	BLANC,
	VERT,
	BLEU,
	ROUGE,
	JAUNE
};

class Forme {
  private:
	static int nbFormes;
	int w;
	int h;
	Point position;
	Couleur couleur;
	int id;

  public:
	Forme();
	Forme(int w, int h, int x, int y, Couleur couleur = Couleur::NOIR);
	Forme(int w, int h, Point position, Couleur couleur = Couleur::NOIR);
	virtual ~Forme();
	static int getNbFormes(void);
	int getW(void) const;
	int getH(void) const;
	void setW(int _w);
	void setH(int _h);
	Point& getPosition(void);
	Couleur getCouleur(void) const;
	void setCouleur(Couleur _couleur);
	void setX(int _x);
	void setY(int _y);
	int getId(void) const;
	static int prochainId(void);
	virtual string toString();
	virtual Forme* clone(void);


};

#endif