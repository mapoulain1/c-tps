#include <iostream>
#include <string>

#include "catch.hpp"
#include "cercle.hpp"
#include "groupe.hpp"
#include "pile.hxx"
#include "utils.hpp"

using std::cout;
using std::endl;
using std::string;

TEST_CASE("max template") {
    int i = 8;
    int j = 12;
    double k = 5.4;

    CHECK(max(i, j) == j);
    // CHECK(max(i, k) == i); :-)
    CHECK(max(i, (int)k) == i);
}

typedef Pile<int> PileInt;
typedef Pile<double> PileDouble;
typedef Pile<string> PileString;

TEST_CASE("Constructeur par defaut") {
    PileInt p; // cela implique que par defaut la capacite de la pile n'est pas
               // nulle => pas d exception

    CHECK(p.empty());
    CHECK(0 == p.size());
}

TEST_CASE("Exceptions de mauvaise construction") {
    REQUIRE_THROWS_AS(PileInt(-1).empty(), std::invalid_argument);
    REQUIRE_THROWS_AS(PileInt(0).empty(), std::invalid_argument);
}

TEST_CASE("Exception pile vide") {
    REQUIRE_THROWS_AS(PileInt().pop(), std::invalid_argument);
}

TEST_CASE("Int pile") {
    PileInt p(10);

    CHECK(p.empty());
    CHECK(0 == p.size());

    p.push(5);

    CHECK(!p.empty());
    CHECK(1 == p.size());
    CHECK(5 == p.top());

    p.push(2);
    p.push(1);

    CHECK(3 == p.size());
    CHECK(1 == p.top());

    p.pop();

    CHECK(2 == p.size());
    CHECK(2 == p.top());

    p.pop();
    p.pop();

    CHECK(0 == p.size());
}

TEST_CASE("Double pile") {
    PileDouble p(10);

    CHECK(p.empty());
    CHECK(0 == p.size());

    p.push(5.5);

    CHECK(!p.empty());
    CHECK(1 == p.size());
    CHECK(5.5 == p.top());

    p.push(2);
    p.push(1);

    CHECK(3 == p.size());
    CHECK(1 == p.top());

    p.pop();

    CHECK(2 == p.size());
    CHECK(2 == p.top());

    p.pop();
    p.pop();

    CHECK(0 == p.size());
}

TEST_CASE("String pile") {
    PileString p(10);

    CHECK(p.empty());
    CHECK(0 == p.size());

    p.push("coucou");

    CHECK(!p.empty());
    CHECK(1 == p.size());
    CHECK("coucou" == p.top());

    p.push("2");
    p.push("4555");

    CHECK(3 == p.size());
    CHECK("4555" == p.top());

    p.pop();

    CHECK(2 == p.size());
    CHECK("2" == p.top());

    p.pop();
    p.pop();

    CHECK(0 == p.size());
}

TEST_CASE("Test clone") {
    Groupe groupe{};
    groupe.add(new Cercle(0, 0, 10));
    groupe.add(new Cercle(-5, 2, 5));
    groupe.add(new Cercle(1, 3, 3));
	cout << groupe.toString() << endl;

	Groupe* cloned = groupe.clone() ;
	cout << cloned->toString() << endl;

	delete cloned;
}