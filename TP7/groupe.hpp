#ifndef GROUPE_HPP
#define GROUPE_HPP

#include "forme.hpp"
#include <vector>
#include <string>

using std::vector;
using std::string;

class Groupe : public Forme {
private:
    vector<Forme*> formes;

public:
    Groupe();
    virtual ~Groupe();
    void add(Forme *forme);
    virtual string toString(void) override;
    Forme* operator[](std::size_t i);
	const Forme* operator[](std::size_t i) const;
    std::size_t size(void) const;
	virtual Groupe* clone(void) override;
    
};


#endif