#ifndef UTILS_HPP
#define UTILS_HPP

#include "utils.hxx"

template <class T>
const T &max(const T &a, const T &b);


#endif