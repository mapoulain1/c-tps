#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "forme.hpp"
#include <string>

using std::string;

class Rectangle : public Forme {
  public:
	Rectangle();
	Rectangle(int x, int y, int w, int h);
	virtual ~Rectangle();
	virtual string toString() override;
	virtual Rectangle* clone(void) override;
};

#endif