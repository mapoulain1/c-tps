#ifndef PILE_HPP
#define PILE_HPP


template <class T>
class Pile {
private: 
    T * array;
    int capacity;
    int topIndex;
public:
    Pile();
    Pile(int capacity);
    ~Pile();

    bool empty(void) const;
    void push(const T& value);
    T& pop(void);
    T& top(void) const;
    int size(void) const;
};


#endif