#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

#include "catch.hpp"
#include "chaine.hpp"

using std::cout;
using std::endl;

TEST_CASE("Constructeur par defaut") {
	Chaine c;
	REQUIRE(-1 == c.getCapacity());
	REQUIRE(nullptr == c.c_str());
}

TEST_CASE("Verification des const sur les accesseurs") {
	const Chaine c;
	CHECK(-1 == c.getCapacity());
	CHECK(nullptr == c.c_str());
}

TEST_CASE("Constructeur par chaine C") {
	char source[] = "rien";
	Chaine c1(source);
	CHECK((signed)strlen(source) == c1.getCapacity());
	CHECK(0 == strcmp(source, c1.c_str()));

	Chaine c2 = "";
	CHECK(0 == c2.getCapacity());
	CHECK(0 == strcmp("", c2.c_str()));
}

TEST_CASE("Constructeur avec capacite") {
	Chaine c1(6);
	Chaine c2((uint)0);

	CHECK(6 == c1.getCapacity());
	CHECK(0 == c2.getCapacity());
	CHECK(0 == strlen(c1.c_str()));
	CHECK(0 == strlen(c2.c_str()));
}

TEST_CASE("Constructeur de copie") {
	Chaine s1(10);
	Chaine s2 = s1;
	CHECK(s1.getCapacity() == s2.getCapacity());
	CHECK((void *)s1.c_str() != (void *)s2.c_str());
	CHECK(0 == strcmp(s1.c_str(), s2.c_str()));
}

TEST_CASE("methode afficher") {
	const char *original = "une chaine a tester\n";
	const Chaine c1(original);
	std::stringstream ss;
	c1.afficher();
	c1.afficher(cout);
	c1.afficher(ss);
	CHECK(ss.str() == original);
	afficherParValeur(c1);
	afficherParReference(c1);
}

TEST_CASE("operateur d'affectation") {
	Chaine s1("une premiere chaine");
	Chaine s2("une deuxieme chaine plus longue que la premiere");

	s1 = s2;

	CHECK(s1.getCapacity() == s2.getCapacity());
	CHECK((void *)s1.c_str() != (void *)s2.c_str());
	CHECK(0 == strcmp(s1.c_str(), s2.c_str()));
	s1 = s1;
}

TEST_CASE("Surcharge <<") {
	const char *chaine = "une nouvelle surcharge";
	Chaine s(chaine);
	std::stringstream ss;
	ss << s;
	CHECK(ss.str() == chaine);
}

TEST_CASE("Surcharge []") {
	const char *chaine = "une nouvelle surcharge";
	Chaine s(chaine);
	CHECK(s[0] == 'u');
	CHECK(s[2] == 'e');
	CHECK(s[11] == 'e');
	s[6] = 'X';
	CHECK(s[6] == 'X');
}

TEST_CASE("Surcharge +") {
	const char *chaine = "une ";
	const char *chaine2 = "chaine";
	Chaine s1 = Chaine{chaine};
	Chaine s2 = Chaine{chaine2};
	auto concat = s1 + s2;
	CHECK(strcmp("une chaine", concat.c_str()) == 0);
}
