#include <iostream>

#include "bavard.hpp"
using std::cout;
using std::endl;

int Bavarde::counter = 0;

Bavarde::Bavarde(int value) : value(value) {
	cout << "Construction de " << value << endl;
	counter++;
}

Bavarde::~Bavarde() {
	cout << "Destruction de " << value << endl;
}

int Bavarde::getValue(void) const {
	return value;
}

int Bavarde::getCounter(void) {
	return counter;
}

void Bavarde::affiche(void) const {
	cout << "Affiche " << value << endl;
}

void fonction(Bavarde b) {
	cout << "code de la fonction " << b.getValue() << endl;
}

// Bavarde globale(39);