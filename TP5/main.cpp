#include <iostream>

#include "bavard.hpp"
#include "cercle.hpp"
#include "forme.hpp"

using std::cout;
using std::endl;

void test1(Bavarde) {
	cout << "appel de fonction avec parametre objet et copie" << endl;
}

Bavarde test2a() {
	cout << "appel de fonction avec retour" << endl;
	return Bavarde();
}

Bavarde test2b() {
	Bavarde b;
	cout << "appel de fonction avec retour" << endl;
	return b;
}

void test3(Bavarde &) {
	cout << "appel de fonction avec référence " << endl;
}

void test4(Bavarde *) {
	cout << "appel de fonction avec un pointeur sur un objet" << endl;
}

void afficher1(Forme f) {
	cout << f.toString() << endl;
}

void afficher2(Forme &f) {
	cout << f.toString() << endl;
}

void afficher3(Forme *f) {
	cout << f->toString() << endl;
}

void testBavard(void) {
	Bavarde b1;
	test1(b1);
	test2a();
	test2b();
	cout << "Nombre de construction : " << Bavarde::getCounter() << endl;
	test3(b1);
	test4(&b1);
	cout << "Nombre de construction : " << Bavarde::getCounter() << endl;
}

void testForme(void) {
	Cercle cercle;
	afficher1(cercle);
	afficher2(cercle);
	afficher3(&cercle);
}

/*
void testChaine(void) {
	Chaine s1;
	Chaine s2(6);
	Chaine s3("essai");
	Chaine s4(s3);
	Chaine s5 = s3;

	s2 = s3;
	s3.remplacer("oups");
	s3.afficher();
	cout << s3.c_str();
	cout << s3[0];
	cout << s3;
}
*/

/*

int main(int, char const *[]) {
	// testBavard();
	// testForme();
	testChaine();
	return 0;
}

*/
