#ifndef BAVARD_HPP
#define BAVARD_HPP

class Bavarde {
  private:
	int value;
	static int counter;

  public:
	Bavarde(int value = 0);
	~Bavarde();
	int getValue(void) const;
	void affiche(void) const;
	static int getCounter(void);
};

void fonction(Bavarde b);

#endif