#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Exemple {
  public:
	void afficher() {
		std::cout << "hello" << std::endl;
	}
};

void exerciceNomPrenom(void) {
	string prenom;
	string nom;
	int age;
	cout << "Quel est votre prénom ?" << endl;
	cin >> prenom;
	cout << "Quel est votre nom ?" << endl;
	cin >> nom;
	cout << "Quel est votre age ?" << endl;
	cin >> age;
	cout << "Bonjour " << prenom << " : " << nom << endl;
}

void exerciceChaines(void) {
	string str1;
	string str2;
	cout << "Première chaine : " << endl;
	cin >> str1;
	cout << "Deuxième chaine : " << endl;
	cin >> str2;

	if (str1 < str2) {
		cout << str1 << " " << str1.length() << endl;
	} else {
		cout << str2 << " " << str2.length() << endl;
	}
}

void exerciceTableau(void) {

	/* on peut utiliser le mot clé const pour définir la taille d'un tableau statique en C++ */
	/* Jamais de #define pour cela */
	const int TAILLE = 10;

	int tab[TAILLE];

	for (int i = 0; i < TAILLE; ++i) {
		tab[i] = i % 2;
		cout << tab[i] << " ";
	}
	cout << endl;
}

void afficher(int a) {
	printf("%d\n", a);
}

void afficher(double a) {
	printf("%lf\n", a);
}

void exerciceChaines2(void) {
	std::string s;
	std::cin >> s;

	std::cout << "#" << s << "#" << std::endl;
	for (long unsigned int i = 0; i < s.length(); ++i)
		std::cout << "@" << s[i] << "@" << (int)s[i] << "@" << std::endl;
}

void fonction1(int a) {
	std::cout << &a << std::endl;
}

void fonction2(int &a) {
	std::cout << &a << std::endl;
}

void exerciceReference(void) {
	int a = 5;
	int &r = a;

	std::cin >> r;
	std::cout << a << endl;
}

void exerciceReference2(void) {
	int a = 5;
	int &r = a;

	fonction1(a);
	fonction2(r);
}

void swap(int &a, int &b) {
	int tmp = a;
	a = b;
	b = tmp;
}

void swap(int *a, int *b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void exerciceSwap(void) {
	int a;
	int b;
	cin >> a;
	cin >> b;
	swap(a, b);
	cout << a << " " << b << endl;
	swap(&a, &b);
	cout << a << " " << b << endl;
}

void exerciceAlloc(void) {
	int *p = new int;
	*p = 3;
	cout << *p << endl;
	delete p;
}

void exerciceAlloc2(void) {
	const int TAILLE = 500;

	int *p = new int[TAILLE];
	auto p2 = new int[TAILLE][500];
	

	for (auto i = 0; i < TAILLE; ++i)
		p[i] = i;
	for (auto i = 0; i < TAILLE; ++i)
		cout << p[i] << endl;

	//delete p;
	delete[] p;
	//delete p2;
	delete[] p2;
}

void exerciceObjet(void) {
	Exemple e;
	e.afficher();
}

int main(int, char **) {
	//exerciceNomPrenom();
	//exerciceChaines();
	//exerciceTableau();
	//afficher(1);
	//afficher(1.0);
	//exerciceChaines2();
	//exerciceReference();
	//exerciceReference2();
	//exerciceSwap();
	//exerciceAlloc();
	exerciceAlloc2();
	//exerciceObjet();
	return 0;
}