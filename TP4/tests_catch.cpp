#include <fstream>
#include <iostream>

#include "catch.hpp"
#include "cercle.hpp"
#include "forme.hpp"
#include "groupe.hpp"
#include "point.hpp"
#include "rectangle.hpp"

using std::cout;
using std::endl;


TEST_CASE("Compteur", "[Forme]") {
	// Pour être correct, ce test doit etre le premier sur Forme
	REQUIRE(0 == Forme::prochainId());
	Forme f1;
	REQUIRE(0 == f1.getId());
	REQUIRE(1 == Forme::prochainId());
	// Verification que la valeur n'est pas decrementee accidentellement.
	Forme *p = new Forme;
	REQUIRE(1 == p->getId());
	delete p;
	REQUIRE(2 == Forme::prochainId());
	
}

TEST_CASE("Composite", "[Groupe]") {
	Groupe g1;
	Forme *f1 = new Cercle{0, 0, 4, 4};
	Forme *f2 = new Cercle{0, 0, 4, 4};
	Forme *f3 = new Rectangle{-8, -2, 6, 8};
	Forme *f4 = new Rectangle{3, 4, 1, 4};

	g1.add(f1);
	g1.add(f2);
	g1.add(f3);
	g1.add(f4);

	REQUIRE(g1.size() == 4);

	cout << g1.toString() << endl;
	
	delete f1;
	delete f2;
	delete f3;
	delete f4;
}

TEST_CASE("Cercle", "[Cercle]") {
	int compteur = Forme::prochainId();
	Cercle c1;
	Cercle c2{0, 0, 4, 4};

	CHECK(c2.getH() == 4);
	CHECK(c2.getW() == 4);
	REQUIRE(c1.toString() == "CERCLE {0,0} 0 0");
	REQUIRE(c2.toString() == "CERCLE {0,0} 4 4");

	c2.setRayon(6);
	REQUIRE(c2.getRayon() == 6);
	REQUIRE(c2.toString() == "CERCLE {0,0} 12 12");
	REQUIRE(c2.getW() == 12);
	REQUIRE(c2.getH() == 12);

	REQUIRE(Forme::prochainId() == (compteur + 2));
}

TEST_CASE("Polymorphisme", "[Forme]") {
	Forme *f1 = new Cercle;
	Forme *f2 = new Rectangle;

	REQUIRE(f1->toString() == "CERCLE {0,0} 0 0");
	REQUIRE(f2->toString() == "RECTANGLE {0,0} 0 0");

	delete f1;
	delete f2;
}

TEST_CASE("Instanciation", "[Point]") {
	Point p1;
	REQUIRE(p1.getX() == 0);
	REQUIRE(p1.getY() == 0);

	p1.setX(11);
	p1.setY(21);

	REQUIRE(p1.getX() == 11);
	REQUIRE(p1.getY() == 21);

	Point p2(12, 22);

	REQUIRE(p2.getX() == 12);
	REQUIRE(p2.getY() == 22);
}

TEST_CASE("Origine", "[Point]") {
	REQUIRE(Point::ORIGINE.getX() == 0);
	REQUIRE(Point::ORIGINE.getY() == 0);
}

TEST_CASE("Instanciation1", "[Forme]") {
	Forme f1;
	REQUIRE(f1.getPosition().getX() == 0);
	REQUIRE(f1.getPosition().getY() == 0);
	REQUIRE(f1.getCouleur() == Couleur::NOIR);
}

TEST_CASE("Instanciation2", "[Forme]") {
	Forme f2;

	f2.setX(15);
	f2.setY(25);
	f2.setCouleur(Couleur::VERT);
	REQUIRE(f2.getPosition().getX() == 15);
	REQUIRE(f2.getPosition().getY() == 25);
	REQUIRE(f2.getCouleur() == Couleur::VERT);
	REQUIRE_FALSE(f2.getCouleur() == Couleur::BLEU);
	REQUIRE_FALSE(f2.getCouleur() == Couleur::ROUGE);
	REQUIRE_FALSE(f2.getCouleur() == Couleur::JAUNE);
}

TEST_CASE("Instanciation3", "[Forme]") {
	Forme f2(10, 10, Point(10, 20), Couleur::ROUGE);

	REQUIRE(f2.getPosition().getX() == 10);
	REQUIRE(f2.getPosition().getY() == 20);
	REQUIRE(f2.getCouleur() == Couleur::ROUGE);
	REQUIRE_FALSE(f2.getCouleur() == Couleur::BLEU);

	f2.getPosition().setX(15);
	f2.getPosition().setY(25);
	f2.setCouleur(Couleur::JAUNE);
	REQUIRE(f2.getPosition().getX() == 15);
	REQUIRE(f2.getPosition().getY() == 25);
	REQUIRE(f2.getCouleur() == Couleur::JAUNE);
	REQUIRE_FALSE(f2.getCouleur() == Couleur::BLEU);
	REQUIRE_FALSE(f2.getCouleur() == Couleur::ROUGE);	
}
