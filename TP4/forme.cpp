#include "forme.hpp"

#include <sstream>
#include <iostream>

using std::cout;
using std::endl;
using std::string;
using std::stringstream;

Forme::Forme() : Forme(0, 0, 0, 0) {
}

Forme::Forme(int x, int y, int w, int h, Couleur couleur) : Forme(w, h, Point{x, y}, couleur) {
}

Forme::Forme(int w, int h, Point position, Couleur couleur) : w(w), h(h), position(position), couleur(couleur), id(nbFormes) {
	nbFormes++;
}

Forme::~Forme() {
}

int Forme::nbFormes = 0;

int Forme::getNbFormes(void) {
	return nbFormes;
}

int Forme::getW(void) const {
	return w;
}

int Forme::getH(void) const {
	return h;
}

void Forme::setW(int _w) {
	w = _w;
}

void Forme::setH(int _h) {
	h = _h;
}

Point &Forme::getPosition(void) {
	return position;
}

Couleur Forme::getCouleur(void) const {
	return couleur;
}
void Forme::setCouleur(Couleur _couleur) {
	couleur = _couleur;
}
void Forme::setX(int _x) {
	position.setX(_x);
}

void Forme::setY(int _y) {
	position.setY(_y);
}

int Forme::getId(void) const {
	return id;
}

int Forme::prochainId(void) {
	return nbFormes;
}

string Forme::toString() {
	stringstream ss;
	ss << "FORME " << position.toString();
	return ss.str();
}
