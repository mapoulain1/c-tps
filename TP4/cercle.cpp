#include "cercle.hpp"
#include "forme.hpp"

#include <sstream>
#include <string>
#include <iostream>

using std::cout;
using std::endl;
using std::string;
using std::stringstream;

Cercle::Cercle() : Cercle(0, 0, 0, 0) {
}

Cercle::Cercle(int x, int y, int w, int h) : Forme(x, y, w, h), rayon(w / 2) {
}

Cercle::Cercle(int x, int y, int rayon) : Cercle(x, y, 2 * rayon, 2 * rayon) {
}

Cercle::~Cercle() {
}

string Cercle::toString() {
	stringstream ss;
	ss << "CERCLE " << getPosition().toString() << " " << getW() << " " << getH();
	return ss.str();
}

int Cercle::getRayon(void) const {
	return rayon;
}

void Cercle::setRayon(int _rayon) {
	rayon = _rayon;
	setW(2 * rayon);
	setH(2 * rayon);
}