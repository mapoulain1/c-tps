#ifndef CERCLE_HPP
#define CERCLE_HPP

#include "forme.hpp"
#include <string>

using std::string;

class Cercle : public Forme {
  private:
	int rayon;

  public:
	Cercle();
	Cercle(int x, int y, int w, int h);
	Cercle(int x, int y, int rayon);
	virtual ~Cercle();
	virtual string toString() override;
	int getRayon(void) const;
	void setRayon(int _rayon);
};

#endif