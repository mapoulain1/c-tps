#include "null_pointer_exception.hpp"
#include "vecteur.hpp"
#include <cstring>
#include <exception>
#include <iostream>
#include <new>
#include <ostream>
#include <stdexcept>

using std::cerr;
using std::cout;
using std::endl;
using std::ostream;

template <class T>
Vecteur<T>::Vecteur(const size_t capacity) try : array(new T[capacity]),
												 _capacity(capacity),
												 _size(0) {

} catch (std::exception &e) {
	cerr << e.what() << endl;
}

template <class T>
Vecteur<T>::Vecteur() : Vecteur(100000) {
}

template <class T>
Vecteur<T>::Vecteur(const Vecteur &v) : Vecteur(v._capacity) {
	if (v.array == nullptr)
		throw null_pointer_exception();
	std::copy(v.array, v.array + v._capacity, array);
	_size = v._size;
}

template <class T>
Vecteur<T>::~Vecteur() {
	delete[] array;
}

template <class T>
void Vecteur<T>::push_back(const T value) {
	if (_capacity == _size) {
		T *newArray = new T[_capacity * 2];
		std::copy(array, array + _capacity, newArray);
		delete[] array;
		array = newArray;
		_capacity *= 2;
	}
	array[_size] = value;
	_size++;
}

template <class T>
Vecteur<T> &Vecteur<T>::operator=(const Vecteur &v) {
	delete[] array;
	array = new T[v._capacity];
	std::copy(v.array, v.array + v._capacity, array);
	_capacity = v._capacity;
	_size = v._size;
	return *this;
}

template <class T>
T &Vecteur<T>::operator[](const size_t i) {
	if (array == nullptr)
		throw null_pointer_exception();
	if (i >= _capacity)
		throw std::out_of_range("can't access index " + i);
	return array[i];
}

template <class T>
T &Vecteur<T>::operator[](const size_t i) const {
	return array[i];
}

template <class T>
size_t Vecteur<T>::capacity(void) const {
	return _capacity;
}

template <class T>
size_t Vecteur<T>::size(void) const {
	return _size;
}

template <class T>
ostream &operator<<(ostream &os, Vecteur<T> &v) {
	os << "[";
	for (size_t i = 0; i < v.size(); i++) {
		os << v[i];
		if (i != v.size() - 1)
			os << ", ";
	}
	os << "]";
	return os;
}

template <class U>
Vecteur<U> operator+(const Vecteur<U> &a, const Vecteur<U> &b) {
	Vecteur<U> res{a._size + b._size};
	std::copy(a.array, a.array + a._size, res.array);
	std::copy(b.array, b.array + b._size, res.array + a._size);
	res._size = a._size + b._size;
	return res;
}