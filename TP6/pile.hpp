#ifndef PILE_H
#define PILE_H


class Pile {
private: 
    int * array;
    int capacity;
    int topIndex;
public:
    Pile();
    Pile(int capacity);
    ~Pile();

    bool empty(void) const;
    void push(const int value);
    int pop(void);
    int top(void) const;
    int size(void) const;


};


#endif