#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

#include "MF.hpp"
#include "catch.hpp"
#include "null_pointer_exception.hpp"
#include "vecteur.hxx"
#include "pile.hpp"

using std::cout;
using std::endl;

/*
TEST_CASE("MF") {
	F f1;
	F f2 = f1;
	f1 = f2;
	A a1;
	A a2 = a1;
}
*/

typedef Vecteur<int> VecteurInt;

TEST_CASE("vecteur int") {

	VecteurInt v1 = {4};
	cout << v1 << endl;
	CHECK(v1.size() == 0);
	CHECK(v1.capacity() == 4);
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);

	CHECK(v1.size() == 4);
	CHECK(v1.capacity() == 4);
	cout << v1 << endl;

	v1.push_back(5);

	CHECK(v1.size() == 5);
	CHECK(v1.capacity() == 8);
	cout << v1 << endl;

	VecteurInt v2 = VecteurInt{v1};
	cout << v2 << endl;

	REQUIRE_THROWS_AS(v1[-1] == 0, std::out_of_range);
	REQUIRE_THROWS_AS(v1[20] == 0, std::out_of_range);
}

TEST_CASE("exception sur pointeur null") {
	VecteurInt s(0);

	std::exception *pe = new null_pointer_exception();
	delete pe;

	REQUIRE_THROWS_AS(s[1] == 0, std::out_of_range);
}

class Bavarde {
	std::string nom;

  public:
	Bavarde(std::string n) : nom(n) {
		std::cout << "constructeur " << nom << std::endl;
	}
	~Bavarde() {
		std::cout << "destructeur " << nom << std::endl;
	}
};

Bavarde g("global");

TEST_CASE("fin de programme") {
	Bavarde t("local");
	static Bavarde s("statlocal");
	// std::exit(1); // => local non détruit
	// std::terminate(); // => rien n'est détruit
	// std::unexpected(); // => rien n'est détruit
}

TEST_CASE("Constructeur par defaut") {
   Pile p; // cela implique que par defaut la capacite de la pile n'est pas nulle => pas d exception
   
   CHECK(  p.empty() );
   CHECK(  0 == p.size() );
}

TEST_CASE("Exceptions de mauvaise construction") {

   REQUIRE_THROWS_AS( Pile(-1).empty(), std::invalid_argument );
   REQUIRE_THROWS_AS( Pile( 0).empty(), std::invalid_argument );
   
}

TEST_CASE("Exception pile vide") {

   REQUIRE_THROWS_AS( Pile().pop(), std::invalid_argument );
}

TEST_CASE("Live pile") {
    Pile p(10);
   
    CHECK(  p.empty() );
    CHECK(  0 == p.size() );

    p.push(5);

    CHECK( !p.empty() );
    CHECK( 1 == p.size() );
    CHECK( 5 == p.top() );

    p.push(2);
    p.push(1);

    CHECK( 3 == p.size() );
    CHECK( 1 == p.top() );
	
    p.pop();

    CHECK( 2 == p.size() );
    CHECK( 2 == p.top() );

    p.pop();
    p.pop();

    CHECK( 0 == p.size() );

}


TEST_CASE("add vector"){
	Vecteur<int> v1 {3};
	Vecteur<int> v2 {5};
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	v2.push_back(4);
	v2.push_back(5);
	v2.push_back(6);
	v2.push_back(7);
	
	Vecteur<int> res = v1 + v2;
	cout << res << endl;
	CHECK(res.size() == 7);
}