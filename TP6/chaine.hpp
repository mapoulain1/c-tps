#ifndef CHAINE_HPP
#define CHAINE_HPP

#include <fstream>
#include <iostream>

class Chaine {
  private:
	int capacity;
	char *tab;

  public:
	Chaine();
	Chaine(const char *src);
	Chaine(uint i);
	Chaine(Chaine const &other);

	~Chaine();

	void afficher(std::ostream &stream = std::cout) const;
	int getCapacity(void) const;
	const char *c_str(void) const;

	Chaine &operator=(const Chaine &);
	char &operator[](std::size_t idx);
	char operator[](std::size_t idx) const;
	friend Chaine operator+(Chaine lhs, const Chaine& rhs);
};

void afficherParValeur(const Chaine c);
void afficherParReference(const Chaine &c);
std::ostream &operator<<(std::ostream &os, const Chaine &chaine);

#endif