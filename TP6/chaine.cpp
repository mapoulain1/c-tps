#include "chaine.hpp"
#include <cstring>
#include <iostream>

using std::cout;
using std::endl;

Chaine::Chaine() : Chaine(nullptr) {
}

Chaine::Chaine(const char *src) : capacity((src == nullptr) ? -1 : strlen(src)),
								  tab((capacity == -1) ? nullptr : new char[capacity + 1]) {
	if (capacity != -1)
		strcpy(tab, src);
}

Chaine::Chaine(uint i) : capacity(i),
						 tab(new char[capacity + 1]) {
	tab[0] = '\0';
}

Chaine::Chaine(Chaine const &other) : Chaine(other.capacity) {
	if (capacity != -1)
		strcpy(tab, other.c_str());
	cout << "Ctor de copie" << endl;
}

void Chaine::afficher(std::ostream &stream) const {
	stream << tab;
}

Chaine::~Chaine() {
	delete[] tab;
}

int Chaine::getCapacity(void) const {
	return capacity;
}

const char *Chaine::c_str(void) const {
	return tab;
}

Chaine &Chaine::operator=(const Chaine &other) {
	if (this != &other) {
		delete[] tab;
		capacity = other.capacity;
		tab = new char[capacity + 1];
		strcpy(tab, other.tab);
	}
	return *this;
}

char &Chaine::operator[](std::size_t idx) {
	return tab[idx];
}

char Chaine::operator[](std::size_t idx) const {
	return tab[idx];
}

void afficherParValeur(const Chaine c) {
	c.afficher();
}
void afficherParReference(const Chaine &c) {
	c.afficher();
}

std::ostream &operator<<(std::ostream &os, const Chaine &chaine) {
	os << chaine.c_str();
	return os;
}


Chaine operator+(Chaine lhs, const Chaine& rhs){
	uint len = 1;
	len += strlen(lhs.tab);
	len += strlen(rhs.tab);
	Chaine concat = Chaine{len};
	strcpy(concat.tab,lhs.tab);
	strcat(concat.tab,rhs.tab);
	return concat;
}