#include <iostream>

using std::cout;
using std::endl;

class M {
  public:
	M() {
		cout << "M::M()" << endl;
	}
	~M() {
		cout << "M::~M()" << endl;
	}
	M(const M &) {
		cout << "M::M(const M&)" << endl;
	}
	M &operator=(const M &) {
		cout << "coucou" << endl;
		cout << "M::operator=(const M&)" << endl;
		return *this;
	}
};

class F : public M {
  public:
	F() {
		cout << "F::F()" << endl;
	}
	~F() {
		cout << "F::~F()" << endl;
	}

	F(const F &f) : M(f) {
		cout << "F::F(const F&)" << endl;
	}
	F &operator=(const F &f) {
		M::operator=(f);
		cout << "F::operator=(const F&)" << endl;
		return *this;
	}
};

class A {
  private:
	M m;

  public:
	A() : m() {
		cout << "A::A()" << endl;
	}
	~A() {
		cout << "A::~A()" << endl;
	}
	A(const A &) : A() {
		cout << "A::A(const A&)" << endl;
	}
	A &operator=(const A &a) {
		m.operator=(a.m);
		cout << "A::operator=(const A&)" << endl;
		return *this;
	}
};