#ifndef VECTEUR_H
#define VECTEUR_H

#include "ostream"

using std::ostream;
template <class T>
class Vecteur {
  private:
	T *array;
	size_t _capacity;
	size_t _size;

  public:
	Vecteur(const size_t capacity);
	Vecteur();
	Vecteur(const Vecteur &v);
	~Vecteur();
	void push_back(const T value);
	Vecteur &operator=(const Vecteur &v);
	T &operator[](const size_t i);
	T &operator[](const size_t i) const;
	size_t capacity(void) const;
	size_t size(void) const;

	template <typename U>
	friend Vecteur<U> operator+(const Vecteur<U> &, const Vecteur<U> &);
};

template <class T>
ostream &operator<<(ostream &os, Vecteur<T> &v);

#endif