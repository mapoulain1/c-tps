#include "pile.hpp"
#include <stdexcept>

Pile::Pile() : Pile(10) {
}

Pile::Pile(int capacity) : array(nullptr), capacity(capacity), topIndex(0) {
	if (capacity <= 0)
		throw std::invalid_argument("capacity can't be negative or null");
	array = new int[capacity];
}

Pile::~Pile() {
	delete[] array;
}

bool Pile::empty(void) const {
	return topIndex == 0;
}

void Pile::push(const int value) {
	if (topIndex >= capacity)
		throw std::overflow_error("stack is full");
	array[topIndex] = value;
	topIndex++;
}

int Pile::pop(void) {
	if (topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	topIndex--;
	return array[topIndex + 1];
}

int Pile::top(void) const {
	if (topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	return array[topIndex - 1];
}

int Pile::size(void) const {
	return topIndex;
}
